package de.geha.pt.uebung05

import spock.lang.Specification

class DualZahlSpec extends Specification {

    DualZahl zahl

    def setup() {
        zahl = new DualZahl()
    }

    def "should be all zeros when initialized"() {
        expect:
        zahl.dZahl == [0, 0, 0, 0, 0, 0, 0, 0]
    }

    def "should be initialized with the right binary array"() {
        given:
        DualZahl zahl = new DualZahl(number)
        expect:
        zahl.getZahl() == binaryNumber
        where:
        number << (0..255).asList()
        binaryNumber = integerToBinaryArray(number)
    }

    def "should be able to be set"() {
        when:
        zahl.setZahl([1, 1, 1, 1, 1, 1, 1, 1] as byte[])
        then:
        zahl.dZahl == [1, 1, 1, 1, 1, 1, 1, 1]
    }

    def "get value that was set before"() {
        given:
        zahl.setZahl([0, 1, 0, 1, 0, 1, 0, 1] as byte[])
        expect:
        zahl.getZahl() == [0, 1, 0, 1, 0, 1, 0, 1]
    }

    def "add another binary number without overflow"() {
        given:
        zahl.setZahl(a as byte[])
        zahl.add(b as byte[])
        expect:
        zahl.getZahl() == c as byte[]
        where:
        [a, b, c] << createBinaryTestIO()
    }

    def "int_to_dual should throw exception on negative number"() {
        when:
        DualZahl.int_to_dual(-2)
        then:
        thrown(IllegalArgumentException)
    }

    def "int_to_dual should throw exception on numbers bigger than eight bits"() {
        when:
        DualZahl.int_to_dual(300)
        then:
        thrown(IllegalArgumentException)
    }

    def "convert positive integer to binary array"() {
        expect:
        DualZahl.int_to_dual(number) == binaryNumber
        where:
        number << (0..255).asList()
        binaryNumber = integerToBinaryArray(number)
    }

    byte[] integerToBinaryArray(int number, int padding = 8) {
        Integer.toBinaryString(number).padLeft(padding, "0").split("").collect { it -> it.toShort() } as byte[]
    }

    List<List<byte[]>> createBinaryTestIO() {
        def list = []
        int upperLimit = 255
        for (a in 0..upperLimit) {
            for (b in 0..upperLimit - a) {
                list << [integerToBinaryArray(a), integerToBinaryArray(b), integerToBinaryArray(a + b)]
            }
        }
        return list
    }
}

package de.geha.pt.uebung07

import spock.lang.Specification

class StationaererPatientSpec extends Specification {

    def "can be initialized with additional parameters"() {
        given:
        String specName = "Tonn"
        String specVorname = "Anton"
        Datum specGeburtsDatum = new Datum(21, 6, 1985)
        String specDiagnose = "Kernassi"
        String specStation = "Gynäkologie"
        int specZimmerNummer = 69
        Datum specAufnahmeDatum = new Datum(01, 01, 2018)
        Datum specEntlassungsDatum = new Datum(14, 01, 2018)
        when:
        StationaererPatient sp = new StationaererPatient(specName, specVorname, specGeburtsDatum, specDiagnose, specStation, specZimmerNummer, specAufnahmeDatum, specEntlassungsDatum)
        then:
        with(ap) {
            name == specName
            vorname == specVorname
            geburtsdatum == specGeburtsDatum
            diagnose == specDiagnose
            station == specStation
            zimmerNummer == specZimmerNummer
            aufnahmeDatum == specAufnahmeDatum
            entlassungsdatum == specEntlassungsDatum
        }
    }
}

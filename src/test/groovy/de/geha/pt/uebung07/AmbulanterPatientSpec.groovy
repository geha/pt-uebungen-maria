package de.geha.pt.uebung07

import spock.lang.Specification

class AmbulanterPatientSpec extends Specification {

    def "can be initialized with additional parameters"() {
        given:
        String specName = "Tonn"
        String specVorname = "Anton"
        Datum specGeburtsDatum = new Datum(21, 6, 1985)
        String specDiagnose = "Kernassi"
        String specHausArzt = "Dr. Wahrheit"
        Datum specBehandlungsDatum = new Datum(29, 2, 2019)
        when:
        AmbulanterPatient ap = new AmbulanterPatient(specName, specVorname, specGeburtsDatum, specDiagnose, specHausArzt, specBehandlungsDatum)
        then:
        with(ap) {
            name == specName
            vorname == specVorname
            geburtsdatum == specGeburtsDatum
            diagnose == specDiagnose
            hausArzt == specHausArzt
            behandlungsDatum == specBehandlungsDatum
        }
    }

    def "shows the right output on print"() {
        given:
        String specName = "Tonn"
        String specVorname = "Anton"
        Datum specGeburtsDatum = new Datum(21, 6, 1985)
        String specDiagnose = "Kernassi"
        String specHausArzt = "Dr. Wahrheit"
        Datum specBehandlungsDatum = new Datum(29, 2, 2019)
        AmbulanterPatient ap = new AmbulanterPatient(specName, specVorname, specGeburtsDatum, specDiagnose, specHausArzt, specBehandlungsDatum)
        when:
        ap.print()
        then:
        PrintStream ps = new PrintStream(new OutputStream() {
            @Override
            void write(int b) throws IOException {

            }
        })

    }
}

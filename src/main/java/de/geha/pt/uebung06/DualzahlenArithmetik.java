package de.geha.pt.uebung06;

import de.geha.pt.uebung05.DualZahl;

public class DualzahlenArithmetik {

    public static void main(String[] args)
    {
        // Konvertierung ohne Instanziierung eines Objekts
        byte[] zahl1 = DualZahl.int_to_dual(123);
        System.out.print("\nErgebnis Konvertierung: ");
        for (int i = 0; i < zahl1.length; i++)
            System.out.print(zahl1[i]);
        // Konvertierung mit Objekt der Klasse DualZahl
        byte[] zahl2 = new byte[8];
        DualZahl dZahl = new DualZahl(123);
        zahl2 = dZahl.getZahl();
        System.out.print("\nZahl 2: ");
        for (int i = 0; i < zahl2.length; i++)
            System.out.print(zahl2[i]);
    }

}

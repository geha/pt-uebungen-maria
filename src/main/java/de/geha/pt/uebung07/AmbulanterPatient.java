package de.geha.pt.uebung07;

public class AmbulanterPatient extends Patient {

    private String hausArzt;

    private Datum behandlungsDatum;

    public AmbulanterPatient(String name, String vorname, Datum geburtsDatum, String diagnose,
            String hausArzt, Datum behandlungsDatum)
    {
        super(name, vorname, geburtsDatum, diagnose);
        this.hausArzt = hausArzt;
        this.behandlungsDatum = behandlungsDatum;
    }

    @Override
    public void print()
    {
        super.print();
        System.out.print("\nHausarzt: " + hausArzt + ", Behandlungsdatum: " + behandlungsDatum.get());
    }
}

package de.geha.pt.uebung07;

public class Patient {

    private String name;

    private String vorname;

    private Datum geburtsdatum;

    private String diagnose;

    public Patient(String name, String vorname, Datum geburtsdatum, String diagnose)
    {
        this.name = name;
        this.vorname = vorname;
        this.geburtsdatum = geburtsdatum;
        this.diagnose = diagnose;
    }

    public void print()
    {
        System.out.print(
                "\n" + vorname + " " + name + ", geb. am " + geburtsdatum.get() + " , Diagnose: " + diagnose);
    }
}

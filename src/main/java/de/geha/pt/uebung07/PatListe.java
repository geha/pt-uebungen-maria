package de.geha.pt.uebung07;

public class PatListe {

    public static void main(String[] args)
    {
        Patient[] liste = new Patient[3];
        liste[0] = new AmbulanterPatient("Schmittchen", "Fritz", new Datum(11, 11, 1911), "Kolitis",
                "Dr. Mabuse", new Datum(31, 5, 2016));
        liste[1] = new StationaererPatient("Mayer", "Karl", new Datum(10, 10, 1961), "Apendizitis",
                "Gastro II", 25, new Datum(12, 5, 2016), new Datum(18, 5, 2016));
        liste[2] = new StationaererPatient("Schneider", "Wilhelmine", new Datum(9, 9, 1963), "Schlaganfall",
                "Neuro I", 25, new Datum(6, 6, 2016), new Datum(0, 0, 0));
        for (int i = 0; i < liste.length; i++)
            liste[i].print();
    }
}

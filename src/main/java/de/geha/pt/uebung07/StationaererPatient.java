package de.geha.pt.uebung07;

public class StationaererPatient extends Patient {

    private String station;

    private int zimmerNummer;

    private Datum aufnahmeDatum;

    private Datum entlassungsDatum;

    public StationaererPatient(String name, String vorname, Datum geburtsDatum, String diagnose,
            String station, int zimmerNummer, Datum aufnahmeDatum, Datum entlassungsDatum)
    {
        super(name, vorname, geburtsDatum, diagnose);
        this.station = station;
        this.zimmerNummer = zimmerNummer;
        this.aufnahmeDatum = aufnahmeDatum;
        this.entlassungsDatum = entlassungsDatum;
    }

    @Override
    public void print()
    {
        super.print();
        System.out.print("\nStation: " + station + ", Zimmer: " + zimmerNummer + ", Aufnahmedatum: "
                + aufnahmeDatum.get() + ", Entlassungsdatum: " + entlassungsDatum.get());
    }
}

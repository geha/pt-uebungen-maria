package de.geha.pt.uebung07;

public class Datum {

    private int tag, monat, jahr;

    public Datum(int tag, int monat, int jahr)
    {
        this.tag = tag;
        this.monat = monat;
        this.jahr = jahr;
    }

    // returns Datum as string
    public String get()
    {
        if (tag == 0 || monat == 0 || jahr == 0)
            return "offen";
        else
            return tag + "." + monat + "." + jahr;
    }
}

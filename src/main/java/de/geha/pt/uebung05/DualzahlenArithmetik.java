package de.geha.pt.uebung05;

public class DualzahlenArithmetik {

    public static void main(String[] args)
    {
        byte[] zahl1 = { 0, 0, 0, 0, 1, 1, 0, 0 };
        byte[] zahl2 = { 0, 0, 0, 0, 1, 1, 0, 1 };
        byte[] zahl3 = new byte[8];
        DualZahl dZahl1 = new DualZahl();
                dZahl1.setZahl(zahl1);
                dZahl1.add(zahl2);
                zahl3 = dZahl1.getZahl();
        System.out.print("Ergebnis: ");
        for (int i = 0; i < zahl3.length; i++)
            System.out.print(zahl3[i]);
    }

}

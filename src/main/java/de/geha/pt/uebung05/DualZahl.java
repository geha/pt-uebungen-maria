package de.geha.pt.uebung05;

public class DualZahl {

    private byte[] dZahl;

    public DualZahl()
    {
        dZahl = new byte[8];
    }

    public DualZahl(int zahl)
    {
        dZahl = int_to_dual(zahl);
    }

    public void setZahl(byte[] zahl)
    {
        dZahl = zahl;
    }

    public byte[] getZahl()
    {
        return dZahl;
    }

    public void add(byte[] zahl)
    {
        byte carry = 0;
        for (int i = 7; i >= 0; i--)
        {
            dZahl[i] = (byte) (dZahl[i] + zahl[i] + carry);
            carry = (byte) (dZahl[i] / 2);
            dZahl[i] = (byte) (dZahl[i] % 2);
        }
    }

    public static byte[] int_to_dual(int zahl)
    {
        if (zahl < 0 || zahl > 255)
            throw new IllegalArgumentException("Invalid value for zahl. Has to be in range from 0 to 255.");

        byte[] result = new byte[8];
        for (int i = 7; i >= 0; i--)
        {
            result[i] = (byte) (zahl % 2);
            zahl /= 2;
        }
        return result;
    }
}
